import React from 'react';
import styles from './App.module.css'
import coronaImage from './media/images/image.png'
import { Grid, Button } from '@material-ui/core';
import Footer from './components/Footer/footer';
import Home from './pages/home/home';
import BdInfo from './pages/bdInfo/BdInfo'
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import HelplineAndEmergency from './pages/helplineAndEmergency/helplineAndEmergency';


function App() {



  return (
    <Router>
      <Grid container>
        <Grid item xs={3} md={3} sm={3}>
          <Link to="/helpline&Emergency" style={{ textDecoration: "none" }}>
            <Button className={styles.InfoBtn} variant="contained" color="primary" style={{ width: "100%" }}>
              helpline & Emergency
        </Button>
          </Link>
        </Grid>
        <Grid item xs={6} md={6} sm={6} style={{ backgroundColor: "aliceblue" }} >
          <Link to="/">
            <img src={coronaImage} className={styles.coronaImage} alt="covid-19" />
          </Link>
        </Grid>
        <Grid item xs={3} md={3} sm={3} style={{ direction: "rtl" }}>
          <Link to="/corona-info/bd" style={{ textDecoration: "none" }}>
            <Button className={styles.InfoBtn} variant="contained" color="secondary" style={{ width: "100%" }}>
              More info about Bangladesh
        </Button>
          </Link>
        </Grid>
        <Grid item xs={12} md={12} sm={12}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/corona-info/bd" component={BdInfo} />
            <Route exact path="/helpline&Emergency" component={HelplineAndEmergency} />
          </Switch>
        </Grid>
        <Grid item xs={12} md={12} sm={12}>
          <Footer />
        </Grid>
      </Grid>
    </Router>
  );
}

export default App;


