import * as React from 'react';
import { Paper } from '@material-ui/core';
import {
    Chart,
    ArgumentAxis,
    ValueAxis,
    AreaSeries,
    Title,
    Legend,
} from '@devexpress/dx-react-chart-material-ui';
import { withStyles } from '@material-ui/core/styles';
import { Animation } from '@devexpress/dx-react-chart';


const format = () => tick => tick;
const legendStyles = () => ({
    root: {
        display: 'flex',
        margin: 'auto',
        flexDirection: 'row',
    },
});
const legendLabelStyles = theme => ({
    label: {
        paddingTop: theme.spacing(1),
        whiteSpace: 'nowrap',
    },
});
const legendItemStyles = () => ({
    item: {
        flexDirection: 'column',
    },
});

const legendRootBase = ({ classes, ...restProps }) => (
    <Legend.Root {...restProps} className={classes.root} />
);
const legendLabelBase = ({ classes, ...restProps }) => (
    <Legend.Label className={classes.label} {...restProps} />
);
const legendItemBase = ({ classes, ...restProps }) => (
    <Legend.Item className={classes.item} {...restProps} />
);
const Root = withStyles(legendStyles, { name: 'LegendRoot' })(legendRootBase);
const Label = withStyles(legendLabelStyles, { name: 'LegendLabel' })(legendLabelBase);
const Item = withStyles(legendItemStyles, { name: 'LegendItem' })(legendItemBase);
const demoStyles = () => ({
    chart: {
        paddingRight: '20px',
    },
    title: {
        whiteSpace: 'pre',
    },
});

const ValueLabel = (props) => {
    const { text } = props;
    return (
        <ValueAxis.Label
            {...props}
            text={`${text}%`}
        />
    );
};

const titleStyles = {
    title: {
        whiteSpace: 'pre',
    },
};
const TitleText = withStyles(titleStyles)(({ classes, ...props }) => (
    <Title.Text {...props} className={classes.title} />
));

class Demo extends React.PureComponent {


    render() {
        const { classes } = this.props;

        return (

            <Paper variant="outlined" square >
                <Chart
                    data={this.props.bdChartInfo}
                    className={classes.chart}
                >
                    <ArgumentAxis tickFormat={format} />
                    <ValueAxis
                        max={50}
                        labelComponent={ValueLabel}
                    />

                    <AreaSeries
                        name="Positive"
                        valueField="Total_Positive"
                        argumentField="Date"
                    />
                    <AreaSeries
                        name="Deaths"
                        valueField="Total_Deaths"
                        argumentField="Date"
                    />

                    <AreaSeries
                        name="Recovered"
                        valueField="Total_Recovered"
                        argumentField="Date"
                    />

                    <Legend position="bottom" rootComponent={Root} itemComponent={Item} labelComponent={Label} />
                    <Title
                        text={`Total Data Visualization \n of \n Death Recover and positive`}
                        textComponent={TitleText}
                    />
                    <Animation />
                </Chart>

            </Paper>
        );
    }
}

export default withStyles(demoStyles, { name: 'Demo' })(Demo);
