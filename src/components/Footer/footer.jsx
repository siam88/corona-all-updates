import React from 'react';
import styles from './footer.module.css'
import Grid from "@material-ui/core/Grid";

import { FaFacebookSquare, FaLinkedin, FaGithubSquare } from "react-icons/fa";
const footer = () => {
    return (
        <div className={styles.container}>
            <div className={styles.background}>
                <Grid container alignItems="center">
                    <Grid item xs={12} md={12} sm={12} className={styles.head}>
                        <h1>Corona Information</h1>
                        <p className={styles.textGap}>It is a voluntary act aimed at mass awareness. All content used here is open to everyone. For any kind of queries you can Email us or create an issue in github. If you like My work please give a star in our <a style={{ color: "deepskyblue" }} href="https://gitlab.com/siam88/corona-all-updates" target="blank" >repository.</a></p>
                        <ul className={styles.icons}>
                            <li>
                                <a href="https://www.facebook.com/rafshanulhoquesiam/" target="blank">
                                    <FaFacebookSquare />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.linkedin.com/in/rafshanul-hoque-siam-22615b14b/" target="blank">
                                    <FaLinkedin />
                                </a>
                            </li>
                            <li>
                                <a href="https://github.com/siam88" target="blank">
                                    <FaGithubSquare />
                                </a>
                            </li>

                        </ul>
                    </Grid>
                    <Grid item md={6} sm={12} xs={12}>
                        <div className={styles.bottomhead}>
                            <h3>Initiative taken by</h3>
                            <div className={styles.hr} />
                            <p className={styleMedia.linkText}>
                                <a href="#">Rafshanul Hoque Siam</a>
                                <br />
                                <br />
                                <a href="#">student at Department of Computer Science Engineering in Southeast University  </a>
                            </p>
                            <br />
                        </div>
                    </Grid>
                    <Grid item md={4} sm={6} xs={12}>
                        <div className={styles.bottomhead}>
                            <h3>Checkout my other Works</h3>
                            <div className={styles.hr} />
                            <p className={styleMedia.linkText}>
                                <a href="#">Havent set it yet</a>
                            </p>

                        </div>

                    </Grid>
                    <Grid item md={12} sm={6} xs={12} >
                        <p className={styles.copyright}>Copyright © 2020 Rafshanul Hoque, made with love</p>
                    </Grid>
                </Grid>


            </div>
        </div >
    );
}

export default footer;
