import React from 'react';
import { Card, CardContent, Typography, Grid, CardMedia } from '@material-ui/core';
import CountUp from 'react-countup';
import cx from 'classnames';
import Lottie from 'react-lottie'
import * as sick from '../../media/sick.json'
import * as recovered from '../../media/revovered.json';
import * as deaths from '../../media/deaths.json'
import useWindowSize from '../../use-window-size';
import LoadingSuspense from '../../components/loadingSuspense/loadingSuspense'
import styles from './Cards.module.css';

const Cards = ({ data, country }) => {
    const size = useWindowSize();


    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: sick.default,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    const defaultOptions2 = {
        loop: true,
        autoplay: true,
        animationData: recovered.default,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    const defaultOptions3 = {
        loop: true,
        autoplay: true,
        animationData: deaths.default,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice'
        }
    };
    if (!data) {
        return <LoadingSuspense />
    }

    const CustomizedCard = ({ type, totalNumber, date, lottieFiles, width }) => {
        return (
            <Grid item component={Card} md={12} className={cx(styles.card, type === "Infected" ? styles.infected : type === "Recovered" ? styles.recovered : styles.deaths)}>
                <CardMedia
                    className={styles.cover}
                >
                    {width > 770 ?
                        <Lottie options={lottieFiles} height={220} width={190} /> :
                        <Lottie options={lottieFiles} height={200} width={150} />
                    }

                </CardMedia>
                <div className={styles.details}>
                    <CardContent className={styles.content}>
                        <Typography color="textSecondary" component="h4" variant="h4" gutterBottom>
                            {type}  <b>{(country) ? " in " + country : null}</b>
                        </Typography>
                        <Typography variant="h5">
                            <CountUp
                                start={0}
                                end={totalNumber}
                                duration={1.5}
                                separator=","
                            />
                        </Typography>

                        <Typography color="textSecondary" gutterBottom  >
                            {date}
                        </Typography>
                        <hr />

                        <Typography variant="body2" >Number of{type} cases of Covid-19</Typography>
                    </CardContent>
                </div>

            </Grid >
        )
    }


    return (
        <div className={styles.container}>
            <Grid container justify="center" alignItems="flex-start"
            >

                <CustomizedCard
                    type="Recovered"
                    totalNumber={data.recovered.value}
                    date={new Date(data.lastUpdate).toDateString()}
                    lottieFiles={defaultOptions2}
                    width={size.width}

                />
                <CustomizedCard
                    type="Infected"
                    totalNumber={data.confirmed.value}
                    date={new Date(data.lastUpdate).toDateString()}
                    lottieFiles={defaultOptions}
                    width={size.width}
                />
                <CustomizedCard
                    type="Deaths"
                    totalNumber={data.deaths.value}
                    date={new Date(data.lastUpdate).toDateString()}
                    lottieFiles={defaultOptions3}
                    width={size.width}
                />

            </Grid>
        </div>

    )
}

export default Cards;