import React, { useState, useEffect } from 'react';
import { NativeSelect, FormControl } from '@material-ui/core'

import styles from './CountryPicker.module.css'

import { fetchCountries } from '../../api'

const CountryPicker = ({ handleCountryChange }) => {

    const [fetchedCountries, setFetchedCountries] = useState([]);

    useEffect(() => {
        const fetchAPI = async () => {
            setFetchedCountries(await fetchCountries())
        }
        fetchAPI();

    }, [setFetchedCountries])


    return (
        <FormControl className={styles.container}>
            <NativeSelect defaultValue="Select a Country" onChange={(e) => handleCountryChange(e.target.value)}  >
                <option value="" className={styles.text}>Global</option>
                {fetchedCountries.map((country, i) =>
                    <option key={i} value={country} className={styles.text}>{country}</option>
                )}
            </NativeSelect>
        </FormControl>
    )
}

export default CountryPicker;