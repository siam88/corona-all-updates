import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import * as mask from '../../media/mask.json';
import * as handwash from '../../media/handwash.json';
import * as doctors from '../../media/doctor.json';
import * as social_distance from '../../media/social_distance.json';
import * as stay_home from '../../media/stay_home.json';
import * as temperatureMeter from '../../media/temperatureMeter.json'
import Slider from 'react-slick';
import Lottie from 'react-lottie';
import useWindowSize from '../../use-window-size';
import { Grid } from '@material-ui/core';

import styles from './Privantions.module.css'


const masks = {
    loop: true,
    autoplay: true,
    animationData: mask.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};
const washing = {
    loop: true,
    autoplay: true,
    animationData: handwash.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};

const doctor = {
    loop: true,
    autoplay: true,
    animationData: doctors.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};
const socialdistance = {
    loop: true,
    autoplay: true,
    animationData: social_distance.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};
const stayhome = {
    loop: true,
    autoplay: true,
    animationData: stay_home.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};
const temp = {
    loop: true,
    autoplay: true,
    animationData: temperatureMeter.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};


const Privantion = () => {
    const size = useWindowSize();
    const CustomLottie = ({ lottieFiles, width, title }) => {
        return (
            <div>
                <div className={styles.image}>

                    {width > 770 ?
                        <Lottie options={lottieFiles} height={220} width={190} /> :
                        <Lottie options={lottieFiles} height={85} width={85} />
                    }
                </div>
                <h3 className={styles.title}>{title}</h3>
            </div>


        )
    }
    return (
        <Grid container
            direction="row"
            justify="center"
            alignItems="flex-start"
            className={styles.container}>


            <Grid item xs={12} md={12} sm={12} >
                <Slider speed={500} slidesToShow={3} slidesToScroll={1} infinite={true} autoplay={true} dots={false} arrows={false} className="slides">
                    <CustomLottie lottieFiles={masks} width={size.width} title="wearing a medical mask can help in limiting the spread" />
                    <CustomLottie lottieFiles={washing} width={size.width} title="wash hand ofter. Use soap and water or an alcohol-based hand rub" />
                    <CustomLottie lottieFiles={doctor} width={size.width} title="Go to doctor  id you have a fever, cough or feel that it is difficult to breathe" />
                    <CustomLottie lottieFiles={socialdistance} width={size.width} title="Keep Your Distance to Slow the Spread" />
                    <CustomLottie lottieFiles={stayhome} width={size.width} title="Stay home. Stay safe. Save lives." />

                    <CustomLottie lottieFiles={temp} width={size.width} title="Don't get close to anyone who has cold or flu-like symptoms" />


                </Slider>
            </Grid>
        </Grid >
    )
}
export default Privantion