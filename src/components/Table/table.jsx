import React, { useState, useEffect } from 'react';

import { Grid } from '@material-ui/core';
import MaterialTable from 'material-table';

export default function MaterialTableDemo({ districtData, titleField, title }) {
    const [state] = React.useState({
        columns: [
            { title: 'Location', field: 'name' },
            { title: 'Total', field: 'value' },

        ],
        data: districtData
    });

    return (
        <MaterialTable
            title={title}
            columns={state.columns}
            data={state.data}

        />
    );
}
