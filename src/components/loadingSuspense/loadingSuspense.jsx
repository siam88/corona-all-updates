import React from 'react';
import { Grid } from '@material-ui/core';
import * as loading from '../../media/Loading.json';
import * as done from '../../media/done.json';
import Lottie from 'react-lottie';
import styles from "./loadingSuspense.module.css"

const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};
const defaultOption2 = {
    loop: true,
    autoplay: true,
    animationData: loading.default,
    rendererSettings: {
        preserveAspectRatio: 'xMidYMid slice'
    }
};

const LoadingSuspense = () => {
    return (
        <div className={styles.loading}>
            <Grid container justify="center">
                <Grid item >
                    <Lottie options={defaultOptions} height={420} width={420} />
                </Grid>
            </Grid>
        </div>
    );
}

export default LoadingSuspense;
