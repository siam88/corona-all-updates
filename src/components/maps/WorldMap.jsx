import React, { useState, useEffect, Fragment } from 'react';
import { Grid } from '@material-ui/core';
import { FaMapMarkerAlt } from "react-icons/fa";
import ReactMapGL, { Marker } from 'react-map-gl';
import * as mapData from '../../data/get-latest.json';

const WorldMap = () => {
    const [countryDataForMap, setCountryDataForMap] = useState();

    const [viewport, setViewport] = useState({
        latitude: 23.8475784,
        longitude: 90.4221037,
        zoom: 2.5,
        width: '100%',
        height: '50vh',
    })


    return (
        <Grid container justify="center">
            <Grid item xs={12} sm={12} md={12}>
                <ReactMapGL
                    {...viewport}
                    mapboxApiAccessToken={process.env.REACT_APP_MAPBOX_TOKEN}
                    mapStyle="mapbox://styles/siam88/ckace1e0769xd1jo2mpsn6sun"
                    onViewportChange={viewport => { setViewport(viewport) }}
                >


                    {mapData.places.map((report, i) => (
                        <Fragment>
                            <Marker
                                key={i}
                                latitude={report.latitude}
                                longitude={report.longitude}
                            >
                                {
                                    countryDataForMap > 20 ? <FaMapMarkerAlt style={{ color: "#ff1600" }} /> : <FaMapMarkerAlt style={{ color: "#955049" }} />

                                }
                            </Marker>
                        </Fragment>

                    ))}
                </ReactMapGL>
            </Grid>
        </Grid>
    );
}

export default WorldMap;
