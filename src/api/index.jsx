import axios from 'axios';

const url = "https://covid19.mathdro.id/api";

const bdUrl = "http://covid19tracker.gov.bd/api/country/latest?onlyCountries=true&iso3=BGD&fbclid=IwAR2XOGpuAgZE2k2WjIDpw3-SQPtVzbNfKXq3NxkR8xSx2dkhkhMh_Z_w-sA"

export const FatchData = async (country) => {
    let changeableUrl = url;
    if (country) {
        changeableUrl = `${url}/countries/${country}`
    }
    try {
        const { data: { confirmed, recovered, deaths, lastUpdate } } = await axios.get(changeableUrl)

        return { confirmed, recovered, deaths, lastUpdate };

    } catch (error) {
        console.log(error)
    }
}

export const fetchDailyData = async () => {
    try {
        const { data } = await axios.get(`${url}/daily`);
        const modifiedData = data.map((dailyData) => ({
            confirmed: dailyData.confirmed.total,
            deaths: dailyData.deaths.total,
            date: dailyData.reportDate
        }))
        return modifiedData
    } catch (Error) {
        console.log(Error)
    }
}

export const fetchCountries = async () => {
    try {
        const { data: { countries } } = await axios.get(`${url}/countries`);
        return countries.map((country) => country.name)
    } catch (Error) {
        console.log(Error);
    }
}

export const fetchBdDistrictInfo = async () => {
    try {
        const { data } = await axios.get(`https://teamtigers.github.io/covid19-dataset-bd/dhakacity/dhakacity.json`);
        return data;
    } catch (Error) {
        console.log(Error);
    }
}

export const fetchBdInfo = async () => {
    try {
        const { data } = await axios.get(`https://coronavirus-19-api.herokuapp.com/countries/bangladesh`);
        return { data };
    } catch (Error) {
        console.log(Error);
    }
}
export const fetchBdChartInfo = async () => {
    try {
        const { data } = await axios.get(`https://jinnatul.github.io/kidProjects/covidBD/bdcovid.json`);
        return { data };
    } catch (Error) {
        console.log(Error);
    }
}
