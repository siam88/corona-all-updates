import React, { useEffect, useState, Fragment } from 'react';
import { Grid } from '@material-ui/core';
import { Cards, Chart, CountryPicker } from '../../components';
import { FatchData, } from '../../api';
import styles from './home.module.css'
import WorldMap from '../../components/maps/WorldMap';
import Privantion from '../../components/Privantions/Privantions';
import LoadingSuspense from '../../components/loadingSuspense/loadingSuspense';

const Home = () => {
    const [data, setData] = useState();
    const [country, setCountry] = useState('');

    const handleCountryChange = async (country) => {
        const fatchData = await FatchData(country);
        setData(fatchData);
        setCountry(country);


    }

    useEffect(() => {
        const getData = async () => {
            setData(await FatchData());

        }
        getData();
    }, [setData]);

    const Header = ({ title }) => {
        return (
            <Grid container justify="center">
                <Grid item xs={6} md={2} sm={6}>
                    <h1 className={styles.header} >{title}</h1>
                </Grid>
            </Grid>
        )
    }
    return (
        <Fragment>
            {!data ? (<LoadingSuspense />) : (
                <Grid container justify="space-evenly" >
                    <Grid item xs={12} md={12} sm={12} >
                        <CountryPicker handleCountryChange={handleCountryChange} />
                    </Grid>
                    <Grid item xs={12} md={5} sm={5}>
                        <Cards data={data} country={country} />
                    </Grid>
                    <Grid item xs={12} md={6} sm={6} >
                        <Chart data={data} country={country} />
                    </Grid>

                    <Grid item xs={12} md={12} sm={12} style={{ marginTop: "5%" }}>
                        <Header title="Privantion" />
                    </Grid>
                    <Grid item xs={12} sm={12} md={8} >
                        <Privantion />
                    </Grid>
                    <Grid item xs={12} md={12} sm={12} style={{ marginTop: "5%" }}>
                        <Header title="Effected Countries" />
                    </Grid>

                    <Grid item xs={12} md={12} sm={12}>
                        <WorldMap />
                    </Grid>
                </Grid >)
            }
        </Fragment >
    );
}

export default Home;
