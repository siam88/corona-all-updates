import React, { useState, useEffect, Fragment } from 'react';
import { Card, CardContent, Typography, Grid } from '@material-ui/core';
import Lottie from 'react-lottie'
import LoadingSuspense from '../../components/loadingSuspense/loadingSuspense';
import { fetchBdDistrictInfo, fetchBdChartInfo, fetchBdInfo } from '../../api'
import useWindowSize from '../../use-window-size';
import styles from './BdInfo.module.css';
import Table from '../../components/Table/table';
import BdChartInfo from '../../components/chart/bdChartsInfo/BdChartInfo';
import { makeStyles } from '@material-ui/core/styles';




const BdInfo = () => {
    const size = useWindowSize();

    const [districtData, setDistrictData] = useState();
    const [bdInfo, setBdInfo] = useState();
    const [bdChartInfo, setBdChartInfo] = useState();


    useEffect(() => {
        const getData = async () => {
            const updatedData = [];
            let data = await fetchBdDistrictInfo();

            for (var key in data) {
                var info = { name: key, value: data[key] };
                updatedData.push(info);
            }
            setDistrictData(updatedData)
        }

        const getBdInfo = async () => {
            const fatchData = await fetchBdInfo();


            setBdInfo(fatchData.data);
        }
        const getBdChartInfo = async () => {
            const fatchData = await fetchBdChartInfo();

            var half_length = Math.ceil(fatchData.data.length / 8);
            var leftSide = fatchData.data.splice(0, half_length);
            setBdChartInfo(leftSide);
        }
        getData();
        getBdInfo();
        getBdChartInfo();
    }, [setDistrictData])


    const Header = ({ title }) => {
        return (
            <Grid container justify="center">
                <Grid item xs={12} md={12} sm={12}>
                    <h1 className={styles.header} >{title}</h1>
                </Grid>
            </Grid>
        )
    }

    return (
        <Fragment >
            {!(districtData && bdInfo && bdChartInfo) ? (<LoadingSuspense />) : (
                <Grid
                    container
                    justify="space-evenly"
                    direction="coloumn"
                    alignItems="flex-start"

                >
                    <Grid item xs={12} md={12} sm={12} >
                        <Header title="COVID-19 in Bangladesh" />
                    </Grid>
                    <Grid item xs={12} sm={12} md={5} >
                        <CustomizedCard bdInfo={bdInfo} />
                    </Grid>
                    <Grid item xs={12} sm={8} md={8} style={{ marginTop: "2%" }}>
                        <Table districtData={districtData} title="Confirmed cases in Dhaka city" />
                    </Grid>

                    <Grid item xs={12} sm={12} md={8} style={{ marginTop: "2%" }}>
                        <BdChartInfo bdChartInfo={bdChartInfo} />
                    </Grid>
                </Grid>)}
        </Fragment>
    );
}

export default BdInfo;


const useStyles = makeStyles({
    root: {
        minWidth: 120,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 13,
    },

});
const CustomizedCard = ({ bdInfo }) => {
    const classes = useStyles();

    return (
        <Grid container justify="center" spacing="2">

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Total Cases</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.cases}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Total Tests</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.totalTests}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Recovered</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.recovered}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Deaths</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.deaths}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Today Deaths</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.todayDeaths}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Critical</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.critical}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Today Cases</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.todayCases}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>


            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Active Cases</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.active}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>


            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b> Tests Per One Million</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            {bdInfo.casesPerOneMillion}
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>


        </Grid>

    )
}