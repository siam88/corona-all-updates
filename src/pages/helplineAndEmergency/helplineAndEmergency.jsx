import React from 'react';
import { Card, CardContent, Typography, Grid, CardMedia, CardActionArea, CardActions, } from '@material-ui/core';
import Faq from '../../components/FAQ/Faq'
import styles from './helplineAndEmergency.module.css';
import { makeStyles } from '@material-ui/core/styles';

const Header = ({ title }) => {
    return (
        <Grid container justify="center">
            <Grid item xs={12} md={12} sm={12}>
                <h1 className={styles.header} >{title}</h1>
            </Grid>
        </Grid>
    )
}

const helplineAndEmergency = () => {
    return (
        <Grid container justify="space-evenly" style={{ marginTop: "1%" }}>
            <Grid item xs={10} md={10} sm={10} >
                <Header title="Q&A on coronaviruses (COVID-19)" />
            </Grid>
            <Grid item xs={10} sm={10} md={10} >
                <Faq />
            </Grid>
            <Grid item xs={10} md={10} sm={10} >
                <Header title="Hotlines & Emergencies" />
            </Grid>
            <Grid item xs={10} sm={10} md={10} style={{ marginTop: "1%" }}>
                <CustomizedCard />
            </Grid>
        </Grid>
    );
}

export default helplineAndEmergency;

const useStyles = makeStyles({
    root: {
        minWidth: 120,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 13,
    },
   
});
const CustomizedCard = () => {
    const classes = useStyles();
    return (
        <Grid container justify="center" spacing="2">

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2" >
                            <b>National Call Center</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            333
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Women & Child Ministry</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            109
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>National Help Line</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            109
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2" >
                            <b>Health Promotion</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            16263
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>

            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>IEDCR</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            10655
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2" >
                            <b>BSMMCH</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            09611677777
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>ICDDRB</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            01401184551,
                            01401184554
                          
                            
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>


            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>BITID, Chattagram</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            244075042
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>



            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2" >
                            <b> Mymensingh Medical College</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            01306497095,
                            01306497096
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2" >
                            <b> Rajshahi Medical College</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            01744595842,
                            01712559673
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Cox's Bazar Medical College</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            01713205877
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
            <Grid item xs={5} md={4} sm={5} >
                <Card className={classes.root}>
                    <CardContent >
                        <Typography className={classes.title} variant="h5" component="h2">
                            <b>Rangpur Medical College</b>
                        </Typography>
                        <Typography className={classes.pos} color="textSecondary">
                            01712177244
                        </Typography>
                    </CardContent>

                </Card>
            </Grid>
        </Grid>

    )
}